function [data,eyeData] = runSingleTrial(td)
% 2016 by Martin Rolfs 
% 2021 by Jan Klanke
% 
% Input:  td      - part of the design structure that pertains to the
%                   trial to be displayed
% 
% Output: data    - trial parameters and behavioral results
%         eyeData - flag for eyetracking  

global scr visual setting keys knob

% clear keyboard buffer
FlushEvents('KeyDown');
if ~setting.TEST == 1, HideCursor(); end
pixx = setting.Pixx;

% Set the transparency for the stimulus (Gabor patch or sine w/ raised
% cosine masketc.)
% Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% predefine boundary information
cxm = td.cen.loc(1);
cym = td.cen.loc(2);
rad = visual.boundRad;
chk = visual.fixCkRad;

% draw trial information on operator screen
if ~setting.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm-chk)*2, (cym-chk)*2, (cxm+chk)*2, (cym+chk)*2); end

% % generate donut shaped stimulus
nStim = length(td.stims.pars.sizp);
sti.tex = visual.procStimsTex ;
sti.vis = td.stims.pars.sizp;
sti.src = [zeros(2,nStim); sti.vis; sti.vis]';
sti.dst = CenterRectOnPoint(sti.src, td.stims.locX, td.stims.locY);
for f = 1:td.totNFr; stiFrames(f).dst = sti.dst + repmat(td.posVec(:,f),2,nStim)'; end

twindow = [0; 0; round(2*scr.ppd);round(2*scr.ppd)]';
twindow = CenterRectOnPoint(twindow, td.stims.locX, td.stims.locY);

% predefine time stamps
tFixaOn = NaN;  % t of fixation stream on
tStimOn = NaN;  % t of stimulus stream on
tStimCf = NaN;  % t of stimulus at full contrast
tStimMS = NaN;  % t of stimulus starting to move
tStimMT = NaN;  % t of stimulus at peak velocity
tStimCd = NaN;  % t of stimulus contrast starts to decline
tStimOf = NaN;  % t of stimulus off
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;

% eyeData
eyeData = [];

% Initialize vector important for response
pressed = 0;
resTier = 1;   
response= NaN(1,3);

% Initialize vector to store data on timing
frameTimes = repmat(12*scr.fd,1,td.totNFr);

% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while firstFlip == 0
    if pixx, firstFlip = PsychProPixx('QueueImage', scr.myimg);
    else     firstFlip = Screen('Flip', scr.myimg); end
end

% set frame count to 0
f = 0;              

% get a first timestap
tLoopBeg = GetSecs; 
t = tLoopBeg;

while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
    for slo = 1:setting.sloMo
        Screen('FillRect', scr.myimg, visual.bgColor);
        % fixation
        if td.fixa.vis(f) 
            % drawFixation(td.cen.col,  td.cen.loc, td.cen.sin, scr.myimg); 
            drawFixation(td.fixa.col, td.fixa.loc, td.fixa.sin, scr.myimg);   
             
        end
        % target
        if td.tar.vis(f)
           drawFixation(td.tar.col, td.tar.loc, td.tar.sin, scr.myimg); 
        end
        % stimulus
        % Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);
        if td.stims.vis(f)
            Screen('DrawTexture', scr.myimg, sti.tex, sti.src, stiFrames(f).dst, td.stims.pars.ori, [], td.stims.amp(:,f), [], [], [], [0, td.stims.pha(:,f), 0, 0]);
        end
        % clockface
        if td.clock.vis(f)  
            Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
            drawClockhandL(td.onPos+f-1, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.handN, scr.myimg);
        end
        % flip
        if pixx; nextFlip = PsychProPixx('QueueImage', scr.myimg);
        else     nextFlip = Screen('Flip', scr.myimg); end
    end
    frameTimes(f) = frameTimes(f) + GetSecs;   
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%
    % Send message that stimulus is now on
    if isnan(tFixaOn) && td.events(f)==1
        if ~setting.TEST  ; Eyelink('message', 'EVENT_FixaOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_FixaOn'); end
        tFixaOn = frameTimes(f);
    end
    if isnan(tStimOn) && td.events(f)==2
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimCf) && td.events(f)==3
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCf'); end
        tStimCf = frameTimes(f);
    end
    if isnan(tStimMS) && td.events(f)==4
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMS'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMS'); end
        tStimMS = frameTimes(f);
    end
    if isnan(tStimMT) && td.events(f)==5
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMT'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMT'); end
        tStimMT = frameTimes(f);
    end
    if isnan(tStimCd) && td.events(f)==6
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCd'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCd'); end
        tStimCd = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==7
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if setting.TEST<2
        [xl,xr,yl,yr] = getCoords;
        if sqrt((xr-cxm)^2+(yr-cym)^2)>chk || sqrt((xl-cxm)^2+(yl-cym)^2)>chk   % check fixation in a circular area FOR BOTH EYES
            fixBreak = 1;
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
    end
end

lastFlip = 0;
while lastFlip == 0 && nextFlip == 0
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
    else     lastFlip = Screen('Flip', scr.myimg); end 
end
tLoopEnd = GetSecs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation 'Blank' %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, PsychProPixx('QueueImage', scr.myimg);
    else     Screen('Flip', scr.myimg); end
end
WaitSecs(td.aftKey/4);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Break trial or initiate reponse query %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nextRespFlip = 0;
lastRespFlip = 0;

% Set init parameters for the rotation knob
ticks   = td.onPosRes;
ticksRaw= 0;
spaKey  = 0;

if setting.knob == 1
    [pressed, valOld] = PsychPowerMate('Get', knob.id);
elseif setting.knob == 2
    knob.encoder.resetCount;
    ticks = knob.encoder.readCount;
    pressed = -readDigitalPin(knob.id,'D7')+1;
end

switch breakIt
    
    case 1
        data = 'fixBreak';
        if setting.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
    
    otherwise
        % Snd('Play',[repmat(0.5,1,1050) linspace(0.5,0.0,50)].*[zeros(1,1000) sin(1:100)],5000);
        % nec. Parameters
        
        tResBeg = GetSecs;
        while ~pressed
            
            % evaluate response to last 2 questions
            if setting.knob == 1

                % get rotary encoder and response tick(s)
                [pressed, valNew] = PsychPowerMate('Get', knob.id);
                
                % adjust speed in speed-mode
                if checkTarPress(keys.resUpArr)
                    speedFactor = 12;
                else
                    speedFactor = 1;
                end
                % transform into onscreen clock rotation
                ticks = ticks + (valNew - valOld) * speedFactor;
                valOld = valNew; 
                
                % excit without time estimate
                if checkTarPress(keys.resSpace); pressed = 1; spaKey = 1; end

            elseif setting.knob == 2

                % get arduino rotary encoder
                count = -readCount(knob.encoder);
                ticks = mod(count,td.respOpt*.5)+1;
                pressed = -readDigitalPin(knob.id,'D7')+1;
                
                % excit without time estimate
                if checkTarPress(keys.resSpace); pressed = 1; spaKey = 1; end
                
            else
                if checkTarPress([keys.resRghtA, keys.resUpArr]); ticks = ticks + 12; end
                if checkTarPress([keys.resLeftA, keys.resUpArr]); ticks = ticks - 12; end
 
                if checkTarPress(keys.resRghtA); ticks = ticks + 1;end
                if checkTarPress(keys.resLeftA); ticks = ticks - 1;end 
                if checkTarPress(keys.resDwArr); pressed = 1; spaKey = 1; end
                if checkTarPress(keys.resSpace); pressed = 1; end
            end
            
            % create response graphics for clock 
            for i = 1:scr.rate
                Screen('FillRect', scr.myimg, visual.bgColor);         % draw background
                Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
                drawClockhandL(ticks, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.handN, scr.myimg); % draw reporthand
                if pixx, PsychProPixx('QueueImage', scr.myimg);
                else Screen('Flip', scr.myimg); end 
            end
            
            % end loop and get response time
            tRes = GetSecs();                
            
        end
        
        % evaluate responses
        if spaKey == pressed
            ticks     = NaN;                                % see below for explanations
            ticksCor  = NaN; 
            ticksCorOn= NaN;
            repDegRaw = NaN;                            
            repDegCor = NaN;
            tResDur   = (tRes - tLoopBeg) * 1000;          % differences in timestamps between the beginn of presentation and pressing of the space bar [ms]
        elseif spaKey ~= pressed
            if ticks < td.onPos                                 % account for the fact that there are 2 clock hands (and you don"t know which is the one
                ticksCor = ticks + td.clock.reportOpt/2;        % reported on by the participant i.e. add respOption/2 if... 
            elseif ticks > td.ofPos                             % a) response position is before onset position and...
                ticksCor = ticks - td.clock.reportOpt/2;        % b) subtract the same if it is after its offset position.
            else
                ticksCor = ticks;
            end
            ticksCorOn = ticksCor - td.onPos;                      % finally, recalculate the position of the clockhand relative to trial onset
            repDegRaw  = mod((ticks / td.clock.reportOpt) * 360,180);    % calculate onset position in degree for UNCORRECTED report index [deg]
            repDegCor  = (ticksCor / td.clock.reportOpt) * 360;    % calculate onset position in degree for CORRECTED report index[deg]
            tResDur = (tRes - tResBeg) * 1000;                     % difference in timestamps between the beginning of the response period and the pressing of the power mate [ms]
        end
        WaitSecs(td.aftKey);

        % reset encoder 
        if setting.knob == 2; knob.encoder.resetCount; end
                
        %%%%%%%%%%%%%%%%%%%%%%%%%
        % feedback presentation %
        %%%%%%%%%%%%%%%%%%%%%%%%%
        if setting.train
            for f = 1:td.febNFr
                for slo = 1:setting.sloMo
                    if isnan(ticks)
                        Screen('FillRect', scr.myimg, visual.bgColor);           % Paint background
                        
                        % let participants know that you want them to
                        % respond /w the rotation knob
                        str = sprintf('Please use the rotation knob!');
                        drawText(str, td.fixa.loc(1), td.fixa.loc(2));           % draw feedback text  
                        if pixx, nextFlip = PsychProPixx('QueueImage', scr.myimg);
                        else nextFlip = Screen('Flip', scr.myimg); end
                    
                    elseif ~isnan(ticks)
                        Screen('FillRect', scr.myimg, visual.bgColor);           % Paint background

                        % draw clock
                        Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
                        
                        % draw actual response
                        drawClockhandL(ticks, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.handN, scr.myimg);  % draw clockhand of participant response
                                                
                        % draw correct response
                        drawClockhandL(td.fbPos, td.clock.handPos, td.clock.handWidth, visual.white, td.clock.handN, scr.myimg); % draw clockhand of correct response
                        if pixx, nextFlip = PsychProPixx('QueueImage', scr.myimg);
                        else nextFlip = Screen('Flip', scr.myimg); end 
                    end
                end
            end
        end
        
        lastFlip = 0;
        while ~lastFlip && ~nextFlip
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
            else lastFlip = Screen('Flip', scr.myimg); end 
        end
        
        for f = 1:scr.rate
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, PsychProPixx('QueueImage', scr.myimg);
            else Screen('Flip', scr.myimg); end 
        end
        
        tClr = GetSecs;
        if setting.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if setting.TEST; fprintf(1,'\nEVENT_Clr'); end 
                
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        condData = sprintf('%i\t%i',...
            [td.expcon td.instru]);                                     % in results, cells  6:8
        
        % collect stimulus information                                        in results, cells 9:20
        stimData = sprintf('%.2f\t%.2f\t%.2f\t%.2f\t%.1f\t%.2f\t%i\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%i\t%.2f\t%.2f',...  
            [td.fixpox td.fixpoy td.tarpox td.tarpoy td.taramp td.tarang td.stiori td.phavel td.appbeg td.apptop td.appdur td.appmdX td.appmdY td.appori td.appvpk td.appamp]);
        
        % collect data on the artificial saccade that shaped the stim.        in results, cells 21:26
        asacData = sprintf('%i\t%.2f\t%.2f\t%.2f\t%.2f',...
            [td.sac.idx td.sac.amp td.sac.vpk td.sac.dur td.sac.ang]);
        
        % collect scale information                                           in results, cells 27:31
        clockData = sprintf('%i\t%i\t%i\t%i',...
            [td.speedSec td.onPos td.onPosDeg td.fbPos]); 
                       
        % collect time information                                            in results, cells 32:39
        timeData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i',...                             
            round(1000*([tFixaOn tStimOn tStimCf tStimMS tStimMT tStimCd tStimOf tRes tClr]-tStimOn)));
        
        % collect response information                                        in results, cells 40:48
        respData = sprintf('%i\t%i\t%.2f\t%.2f\t%.2f',...                              
            [spaKey ticks repDegRaw repDegCor tResDur]);      

        % get information about how timing of frames worke                    in results, cells 44:45
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);        
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [3 x condData, 10 x trialData, 5 x sacData, 10 x timeData %i, 8 x respData, 2 x frameData]
        data = sprintf('%s\t%s\t%s\t%s\t%s\t%s\t%s',...
            condData, stimData, asacData, clockData, timeData, respData, frameData); 

end
end