function drawClockhandL(p, VecXY, width, col, n, windowptr)
% 2019 by Jan Klanke

% ensure that p cannot be 0
p(p == 0) = 1;

% adapt locations in case more than one clock hand needs to be drawn
if n > 1, p = p: length(VecXY) / n: length(VecXY) + p-1; end

% correct for entries that are too big
p(p > length(VecXY)) = p(p > length(VecXY)) - (length(VecXY) * floor(p(p > length(VecXY)) / length(VecXY)));
p(p <= 0)            = p(p <= 0)            - (length(VecXY) * floor(p(p <= 0)            / length(VecXY)));
p(p == 0)            = p(p == 0) + 1; 

% re-size vector with x and y columns of line elements 
vec = reshape(VecXY(:,p), [2, 2*n]);

% draw lines...
Screen('DrawLines', windowptr, vec, width, col);
end