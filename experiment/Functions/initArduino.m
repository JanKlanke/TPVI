function prepArduino(port)

% prepare sound
% by JK 2022
% 
%

global setting knob

if ~setting.knob; fprintf(1,'\n\nWarning, knob not enabled. proceed with caution...\n'); return; end    

knob.id      = arduino(port,'Uno','Libraries','rotaryEncoder'); % init arduino
knob.encoder = rotaryEncoder(knob.id,'D2','D3',180);            % define as rotary encoder    
configurePin(knob.id,'D7','Pullup');                            % define pushbutton



end