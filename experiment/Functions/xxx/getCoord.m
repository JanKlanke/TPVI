function [x,y] = getCoord

global setting scr

if setting.TEST
	[x,y,~] = GetMouse( scr.main );         % get gaze position from mouse							
else
	evt = Eyelink( 'newestfloatsample');	
	x   = evt.gx(setting.DOMEYE)/2;			
	y   = evt.gy(setting.DOMEYE)/2;			
end
