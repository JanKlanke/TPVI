function re = initPowerMate()

% Initialize the Powermate knob
% by JK 2022
% 
% Output:
%           re - device handle
% 


% get list with all available power mates (should not be greater than 1 tbh
% but you never know)
deviceIds = PsychPowerMate('List');                 

% create handle 're' for the device 
re        = PsychPowerMate('Open', deviceIds, 10000);

% set brightness of the knob led to 0
level      = 0;
PsychPowerMate('SetBrightness', re, level); % Change brightness of the LED of the PowerMate specified by handle 're' to 'level' var. Level can be between 0 and 255.

end