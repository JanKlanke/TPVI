%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Temporal Processing of stimuli briefly stabilized on the retina by Voluntarty vs Involuntary eye movements   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 2024 by Jan Klanke
%
% 
% DESKRIPTION
%
%
%
% TODOs
%
%
%

clear all;
clear mex;
clear functions; 

addpath('Functions/','Data/','Data/msCoordinates/','Data/mainSequences/');

home;
expStart=tic;


global setting visual scr keys knob %#ok<*NUSED>

setting.TEST = 2;         % test in dummy mode? 0=with eyelink; 1=mouse as eye; 2=no gaze position checking of any sort
setting.train= 0;         % do you want to run a practice block? (0 = no practice--real experiment, 1= practice block)
setting.alexandra = 0;    % present stimulus only over right part of the screen for alexandra :) 

setting.sloMo= 1;         % to play stimulus in slow motion, draw every frame setting.sloMo times

setting.pilot= 1;         % are you currently piloting (1) or not (0)--only important w/ respect to the tenacity with which we ask for participant info
setting.Pixx = 1;         % are you using the propixx (1) or not (0) or do you want to simulate the propixx (2)
setting.knob = 2;         % Do you use arrow keys (0) or the Griffith PowerMate (1) or Arduino (2)
if setting.alexandra
    setting.MODE = 1;    % test monocularly or binocularly? 1= monocular, 2= binocular
else
    setting.MODE = 2;
end

exptname = 'TPVI';

try 
    newFile = 0;
    while ~newFile
        [vpno, seno, cond, dome, subjectCode, overrideSwitch] = prepExp(exptname);
        subjectCode = strcat('Data/',subjectCode);
        
         
        % create data file
        datFile = sprintf('%s.dat',subjectCode);
        if exist(datFile,'file')
            o = input('>>>> This file exists already. Should I overwrite it [y / n]? ','s');
            if strcmp(o,'y')
                newFile = 1;
            end
        else
            newFile = 1;
        end
    end
    
    % prep rotation knob
    if setting.knob == 1
        knob.id = initPowerMate;
    elseif setting.knob == 2
       initArduino('/dev/ttyACM0');      
    end
    
    % prepare replay condition
    [ms, stop] = prepReplayCondition(vpno, seno, overrideSwitch);
    if stop; return; end
    
    % prepare screens
    prepScreen;
   
    % get key assignment
    getKeyAssignment;
    
    % disable keyboard
    ListenChar(2);
          
    % prepare stimuli
    prepStim;
    
    % generate design
    design = genDesign(vpno, seno, cond, dome, ms, subjectCode);
    
    % initialize eyelink-connection
    if setting.TEST<2
        [el, err]=initEyelinkNew(subjectCode(6:end));
        if err==el.TERMINATE_KEY
            return
        end
    else
        el=[];
    end
    
    % runtrials
    design = runTrials(design,datFile,el);
    
    % shut down everything
    reddUp;
    
catch me
    rethrow(me); 
    reddUp; %#ok<UNRCH>
end

expDur=toc(expStart);

fprintf(1,'\n\nThis (part of the) experiment took %.0f min.',(expDur)/60);
fprintf(1,'\n\nOK!\n');

% plotReplayResults(exptname, vpno, seno);